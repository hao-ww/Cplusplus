#pragma once
#include"timer.h"
#include"vector2.h"

class Camera
{
public:
	Camera()
	{
		timer_shake.set_one_shot(true);
		timer_shake.set_callback([&]()
			{
				is_shaking = false;
				reset();
			});
	}
	~Camera() = default;

	const Vector2& get_position() const
	{
		return position;
	}

	void reset()
	{
		position.x = 0;
		position.y = 0;
	}

	void on_update(int delta)
	{
		timer_shake.on_update(delta);
		if (is_shaking)
		{
			position.x = (-50 + rand() % 100) / 50.0f * shaking_strenth;
			position.y = (-50 + rand() % 100) / 50.0f * shaking_strenth;
		}
	}

	void shake(float strength, int duration)
	{
		is_shaking = true;
		shaking_strenth = strength;
		timer_shake.set_wait_time(duration);
		timer_shake.restart();
	}
private:
	Timer timer_shake;		//摄相机抖动定时器
	Vector2 position;		//摄像机位置
	bool is_shaking = false;//是否抖动
	float shaking_strenth = 0;//摄相机抖动幅度
};

