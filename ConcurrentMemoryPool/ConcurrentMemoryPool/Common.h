#pragma once


#include<iostream>
#include<vector>
#include<assert.h>
using std::cout;
using std::endl;

static const size_t MAX_BYTES = 256 * 1024;

void*& NextObj(void* obj)
{
	return *(void**)obj;
}

//管理切分好的对象的自由链表
class FreeList
{
public:
	void Push(void* obj)
	{
		assert(obj);

		//头插
		NextObj(obj) = _freeList;
		_freeList = obj;
	}
	void* Pop()
	{
		assert(_freeList);
		void* obj = _freeList;
		_freeList = NextObj(obj);
	}
private:
	void* _freeList;
};

//计算对象大小的对齐映射规则
class SizeClass
{
public:

private:

};